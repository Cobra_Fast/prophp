<?php

class SimpleXMLHelper
{
	public static function ToArray(SimpleXMLElement $sxmle)
	{
		return self::ToArray_Worker($sxmle);
	}
	
	private static function ToArray_Worker(SimpleXMLElement $parent, $recur = 0)
	{
		$tmp = array();
	
		$ns = $parent->getNamespaces(true);
		$ns[''] = '';
		
		foreach ($ns as $prefix => $ns)
		{
			foreach ($parent->children($ns) as $child)
			{
				$new = array(
					'name' => (strlen($ns) > 0 ? $prefix . ':' : '') . $child->getName(),
					'text' => trim((string)$child)
				);
				
				$attributes = self::extractAttributes($child);
				if (count($attributes) > 0)
					$new['attributes'] = $attributes;
				
				$children = self::ToArray_Worker($child, $recur + 1);
				if (count($children) > 0)
					$new['children'] = $children;
				
				$tmp[] = $new;
			}
		}

		if ($recur == 0)
		{
			$tmp2 = array();
			$tmp2['name'] = $parent->getName();
			$tmp2['attributes'] = self::extractAttributes($parent);
			if (count($tmp2['attributes']) <= 0) unset($tmp2['attributes']);
			$tmp2['children'] = $tmp;
			if (count($tmp2['children']) <= 0) unset($tmp2['children']);
			$tmp = $tmp2;
		}

		return $tmp;
	}
	
	private static function extractAttributes(SimpleXMLElement $sxmle)
	{
		$result = array();
		
		$ns = $sxmle->getNamespaces(true);
		$ns[''] = '';
		
		foreach ($ns as $prefix => $ns)
			foreach ($sxmle->attributes($ns) as $key => $val)
				$result[(strlen($prefix) > 0 ? $prefix . ':' : '') . $key] = (string)$val;
		
		return $result;
	}
}