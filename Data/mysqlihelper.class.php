<?php

class MySQLiHelper
{
	public $sqli;

	public function __construct(mysqli $sqli)
	{
		$this->sqli = $sqli;
	}
	
	/**
	 * Executes a query and returns the "result" or FALSE
	 * @return mixed
	 */
	public function Query()
	{
		$args = func_get_args();
		if (count($args) == 1)
			return $this->sqli->query($args[0]);
		else if (count($args) > 1)
		{
			$stmt = $this->sqli->prepare($args[0]);
			if ($stmt === false)
				return false;
			$types = '';
			$param = array();
			for ($i = 1; $i < count($args); $i++)
			{
				$types .= $this->GetType($args[$i]);
				$param[] = &$args[$i];
			}
			array_unshift($param, $types);
			call_user_func_array(array($stmt, 'bind_param'), $param);
			if ($stmt->execute() === false)
				return false;
			if (strpos($args[0], 'INSERT') === 0)
				return $stmt->insert_id;
			$yield = $stmt->get_result();
			$stmt->close();
			return $yield;
		}
		else
			return false;
	}
	
	private function GetType($var)
	{
		if (is_int($var))
			return 'i';
		else if (is_float($var))
			return 'd';
		else if (is_string($var))
			return 's';
		else
			return 'b';
	}
	
	/**
	 * Returns the underlying mysqli instance
	 * @return mysqli underlying mysqli instance
	 */
	public function GetLink()
	{
		return $this->sqli;
	}
}
