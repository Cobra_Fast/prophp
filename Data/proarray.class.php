<?php

class ProArray implements ArrayAccess, Countable
{
	private $array;

	public function __construct($array = array())
	{
		if (!is_array($array))
		{
			trigger_error('ProArray not instantiated from an array. Instantiating with a single element array.');
			$this->array = array($array);
		}
		else
			$this->array = array_values($array);
	}

	public function All(callable $callback) : bool
	{
		foreach ($this->array as $value)
			if (!$callback($value))
				return false;
		return true;
	}

	public function Any(callable $callback) : bool
	{
		foreach ($this->array as $value)
			if ($callback($value))
				return true;
		return false;
	}

	public function First(callable $callback = null)
	{
		if ($callback == null)
			return $this->array[0];

		foreach ($this->array as $value)
			if ($callback($value))
				return $value;
		return null;
	}

	public function Last(callable $callback = null)
	{
		if ($callback == null)
			return $this->array[$this->Count() - 1];
		
		foreach (array_reverse($this->array) as $value)
			if ($callback($value))
				return $value;
		return null;
	}

	public function Single(callable $callback = null)
	{
		$array = $callback == null
			? $this->array
			: $this->Where($callback);
		if (count($array) == 1)
			return $array[0];
		trigger_error('Result is either empty or more than 1.', E_USER_WARNING);
		return null;
	}

	public function Select(callable $callback) : ProArray
	{
		$result = array();
		foreach ($this->array as $value)
			$result[] = $callback($value);
		return new self($result);
	}

	public function Where(callable $callback) : ProArray
	{
		$result = array();
		foreach ($this->array as $value)
			if ($callback($value))
				$result[] = $value;
		return new self($result);
	}

	public function Each(callable $callback) : void
	{
		foreach ($this->array as $value)
			$callback($value);
	}

	public function Reverse() : ProArray
	{
		return new self(array_reverse($this->array));
	}

	public function Distinct() : ProArray
	{
		return new self(array_unique($this->array));
	}

	public function Max()
	{
		$max = null;
		foreach ($this->array as $value)
			if ($max === null || $value > $max)
				$max = $value;
		return $max;
	}

	public function Min()
	{
		$min = null;
		foreach ($this->array as $value)
			if ($min === null || $value < $min)
				$min = $value;
		return $max;
	}

	public function Average()
	{
		$acc = 0;
		$cnt = 0;
		foreach ($this->array as $value)
		{
			$acc += $value;
			$cnt++;
		}
		return $acc / (double)$cnt;
	}

	public function Aggregate($seed, callable $callback)
	{
		foreach ($this->array as $index => $value)
			$seed = $callback($seed, $value, $index);
		return $seed;
	}

	public function ToArray() : array
	{
		return $this->array;
	}

	// ArrayAccess impl
	public function offsetSet($offset, $value)
	{
		$this->array[$offset] = $value;
	}

	public function offsetExists($offset)
	{
		return isset($this->array[$offset]);
	}

	public function offsetUnset($offset)
	{
		unset($this->array[$offset]);
	}

	public function offsetGet($offset)
	{
		return $this->array[$offset];
	}

	// Countable impl
	public function count()
	{
		return count($this->array);
	}
}
