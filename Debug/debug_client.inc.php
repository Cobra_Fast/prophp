<?php

class Debug
{
	private static function Connect()
	{
		if (!defined('DEBUG_SERVER_ADDRESS'))
			return false;
		
		$fp = stream_socket_client(DEBUG_SERVER_ADDRESS, $errno, $errstr);
		if (!$fp)
		{
			//trigger_error($errno . ': ' . $errstr);
			return false;
		}
		
		return $fp;
	}
	
	public static function Write($object, $appendln = true, $fancy = true)
	{
		$fp = Debug::Connect();
		if (!$fp) return;
		
		if ($fancy && count($bt = debug_backtrace()) > 1)
		{
			fwrite($fp, "[");
			if (isset($bt[1]['class']))
			{
				$fc = (strlen($bt[1]['class']) % 10);
				fwrite($fp, "\x1b[" . (($fc > 4 ? $fc/2 : $fc) + 32) . ($fc > 4 ? ';1' : '') . "m" . $bt[1]['class'] . "\x1b[0m::");
			}
			$fc = (strlen($bt[1]['function']) % 10);
			fwrite($fp, "\x1b[" . (($fc > 4 ? $fc/2 : $fc) + 32) . ($fc > 4 ? ';1' : '') . "m" . $bt[1]['function'] . "\x1b[0m");
			fwrite($fp, "]: ");
		}
		
		if (is_string($object))
		{
			fwrite($fp, $object . ($appendln ? "\n" : ''));
		}
		else if (is_array($object) || is_object($object))
		{
			fwrite($fp, print_r($object, true) . ($appendln ? "\n" : ''));
		}
		else if (is_resource($object))
		{
			fwrite($fp, 'Resource' . ($appendln ? "\n" : ''));
		}
		
		fclose($fp);
	}
}
