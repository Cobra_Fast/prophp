<?php
if (!defined('STDOUT')) die('This needs to be run on CLI!');

$port = 50252;

$socket = stream_socket_server("tcp://0.0.0.0:$port", $errno, $errstr);
if (!$socket)
	die($errno . ': ' . $errstr . ' @' . __LINE__);
else
	echo 'Socket opened, listening for requests.' . "\n\n";

while (true)
{
	if (is_resource($conn = @stream_socket_accept($socket)))
	{
		echo '[' . date('r') . ']: ';
		while (!feof($conn))
			echo fread($conn, 4096);
		fclose($conn);
	}
}