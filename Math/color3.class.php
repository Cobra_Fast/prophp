<?php
require_once(dirname(__FILE__) . '/vector3.class.php');

class Color3 extends Vector3
{
	public function Normalize()
	{
		$this->x = min(array(1, max(array(0, abs($this->x)))));
		$this->y = min(array(1, max(array(0, abs($this->y)))));
		$this->z = min(array(1, max(array(0, abs($this->z)))));
		
		return $this;
	}
	
	public function To255()
	{
		return array(
			'r' => round($this->x * 255.0),
			'g' => round($this->y * 255.0),
			'b' => round($this->z * 255.0)
		);
	}
	
	public function To255Hex()
	{
		return '#'
			. str_pad(dechex(round($this->x * 255.0)), 2, '0', STR_PAD_LEFT)
			. str_pad(dechex(round($this->y * 255.0)), 2, '0', STR_PAD_LEFT)
			. str_pad(dechex(round($this->z * 255.0)), 2, '0', STR_PAD_LEFT);
	}
	
	public static function FromHex($hex)
	{
		if ($hex[0] == '#')
			$hex = substr($hex, 1);
		
		$r = 0;
		$g = 0;
		$b = 0;
		if (strlen($hex) == 3)
		{
			$r = hexdec($hex[0]) / 15.0;
			$g = hexdec($hex[1]) / 15.0;
			$b = hexdec($hex[2]) / 15.0;
		}
		else if (strlen($hex) == 6)
		{
			$r = hexdec(substr($hex, 0, 2)) / 255.0;
			$g = hexdec(substr($hex, 2, 2)) / 255.0;
			$b = hexdec(substr($hex, 4, 2)) / 255.0;
		}
		else
			trigger_error('Unsupported format/data passed to FromHexRGB().', E_USER_ERROR);
		
		return new Color3($r, $g, $b);
	}
	
	public static function FromByte($r, $g, $b)
	{
		return new Color3($r / 255.0, $g / 255.0, $b / 255.0);
	}
	
	public static function FromFloat($r, $g, $b)
	{
		return new Color3($r, $g, $b);
	}
}