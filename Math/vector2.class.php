<?php

class Vector2
{
	public $x;
	public $y;
	
	public function __construct($x, $y = false)
	{
		if ($x instanceof Vector2)
		{
			$this->x = $x->x;
			$this->y = $x->y;
		}
		else if (is_numeric($x))
		{
			$this->x = $x;
			if (is_numeric($y))
				$this->y = $y;
			else
				$this->y = $x;
		}
		else
			trigger_error('Unsupported construction types.', E_USER_ERROR);
	}
	
	public function Length()
	{
		return sqrt($this->x * $this->x + $this->y * $this->y);
	}
	
	public function IsZero()
	{
		return ($this->x == 0 && $this->y == 0);
	}
	
	public function Dot(Vector2 $v)
	{
		return $this->x * $v->x + $this->y * $v->y;
	}
	
	public function Multiply($n)
	{
		if ($n instanceof Vector2)
		{
			$this->x = $this->x * $n->x;
			$this->y = $this->y * $n->y;
		}
		else if (is_numeric($n))
		{
			$this->x *= $n;
			$this->y *= $n;
		}
		else
			trigger_error('Unsupported operand type for vector multiplication.', E_USER_ERROR);
		
		return $this;
	}
	
	public function Divide($n)
	{
		if ($n instanceof Vector2)
		{
			$this->x = $this->x / $n->x;
			$this->y = $this->y / $n->y;
		}
		else if (is_numeric($n))
		{
			$this->x /= $n;
			$this->y /= $n;
		}
		else
			trigger_error('Unsupported operand type for vector multiplication.', E_USER_ERROR);
		
		return $this;
	}
	
	public function Add($n)
	{
		if ($n instanceof Vector2)
		{
			$this->x = $this->x + $n->x;
			$this->y = $this->y + $n->y;
		}
		else if (is_numeric($n))
		{
			$this->x += $n;
			$this->y += $n;
		}
		else
			trigger_error('Unsupported operand type for vector addition.', E_USER_ERROR);
		
		return $this;
	}
	
	public function Substract($n)
	{
		if ($n instanceof Vector2)
		{
			$this->x = $this->x - $n->x;
			$this->y = $this->y - $n->y;
		}
		else if (is_numeric($n))
		{
			$this->x -= $n;
			$this->y -= $n;
		}
		else
			trigger_error('Unsupported operand type for vector substraction.', E_USER_ERROR);
		
		return $this;
	}
	
	public function Inverse()
	{
		$this->x = 1.0 / $this->x;
		$this->y = 1.0 / $this->y;
	}
	
	public function Negate()
	{
		$this->x = -$this->x;
		$this->y = -$this->y;
	}
	
	public static function sMultiply(Vector2 $vec, $n)
	{
		$result = clone $vec;
		return $result->Multiply($n);
	}
	
	public static function sDivide(Vector2 $vec, $n)
	{
		$result = clone $vec;
		return $result->Divide($n);
	}
	
	public static function sAdd(Vector2 $vec, $n)
	{
		$result = clone $vec;
		return $result->Add($n);
	}
	
	public static function sSubstract(Vector2 $vec, $n)
	{
		$result = clone $vec;
		return $result->Substract($n);
	}
}