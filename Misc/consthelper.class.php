<?php

class ConstHelper
{
	/**
	 * Define a constant if it isn't already defined. Good for setting defaults.
	 * @param string $name
	 * @param mixed $value
	 * @return void
	 */
	public static function DefineDefault($name, $value)
	{
		if (!defined($name))
			define($name, $value);
	}

	/**
	 * Read a constant or return $default_value if it isn't set.
	 * @param string $name
	 * @param mixed $default_value
	 * @return void
	 */
	public static function ReadDefault($name, $default_value = false)
	{
		if (defined($name))
			return constant($name);
		return $default_value;
	}

	/**
	 * Check if a constant exists and test it.
	 * @param string $name
	 * @param string $callback
	 * @return boolean TRUE if constant exists and tests pass, FALSE otherwise.
	 */
	public static function Test($name, $callback)
	{
		return defined($name) && call_user_func($callback, constant($name));
	}
}