<?php

/**
 * This class builds extensive indexes for all or supplied properties.
 * With this class, searching all objects with a certain property value is 5-20X faster that a regular foreach() search.
 */
class IndexedObjectList
{
	private $a;
	private $i;
	private $properties = array();
	
	/**
	 * @param array(object) $aObject Array of objects of the same type to initialize this list with.
	 * @param array(string) $properties Names of the properties to index, if left out nothing will be indexed.
	 */
	public function __construct($aObject, $properties = array())
	{
		if (!is_array($aObject))
			trigger_error('Array must be objects and of the same class.', E_USER_ERROR);
		
		$fi = array_keys($aObject);
		if (count($fi) <= 0 || empty($fi))
		{
			$this->a = array();
			$this->i = array();
			foreach ($properties as $p)
				$this->i[$p] = array();
			$this->properties = $properties;
			return;
		}
		
		$fi = $fi[0];
		
		if (!is_object($aObject[$fi]))
			trigger_error('Array must be objects and of the same class.', E_USER_ERROR);
		
		$type = get_class($aObject[$fi]);
		foreach ($aObject as $o)
		{
			if (!is_object($o) || get_class($o) != $type)
				trigger_error('Array must be objects and of the same class.', E_USER_ERROR);
		}
		
		$this->a = $aObject;
		$this->i = null;
		$this->properties = $properties;
		$this->buildIndex();
	}
	
	/**
	 * Get the array of indexed objects in this list.
	 */
	public function GetArray()
	{
		return $this->a;
	}
	
	/**
	 * Add an item to this list and rebuild the index.
	 * Instanciating this class from an array is much faster, though.
	 * @param object $o The object to add
	 * @param false|string $key A custom key for the object. (Good for replacing)
	 */
	public function Add($o, $key = false)
	{
		$fi = array_keys($this->a);
		$fi = $fi[0];
		
		if (is_object($o) && get_class($o) == get_class($this->a[$fi]))
		{
			if ($key === false)
				$this->a[] = $o;
			else
				$this->a[$key] = $o;
			$this->buildIndex();
		}
		else
			return false;
	}
	
	/**
	 * Searches through the list for objects with a specific property value and returns findings.
	 */
	public function GetAllByProperty($property, $value)
	{
		if (isset($this->i[$property]))
		{
			if (isset($this->i[$property][$value]))
				return $this->i[$property][$value];
			else
				return array();
		}
		else
		{
			// scan unindexed properties slowly
			$r = array();
			foreach ($this->a as $o)
			{
				if ($o->$property == $value)
					$r[] = $o;
			}
			return $r;
		}
	}
	
	private function buildIndex()
	{
		$fi = array_keys($this->a);
		$fi = $fi[0];
		
		if (empty($this->properties))
			$this->properties = array_keys(get_object_vars($this->a[$fi]));
		
		$this->i = array();
		foreach ($this->properties as $property)
		{
			$pi = array();
			foreach ($this->a as $key => $o)
				$pi[$o->$property][] = &$this->a[$key];
			$this->i[$property] = $pi;
		}
	}
	
	/**
	 * Print out some statistics...
	 */
	public function PrintStats()
	{
		echo count($this->a) . ' objects managed.' . "\n";
		
		foreach ($this->i as $property => $index)
		{
			foreach ($index as $value => $i)
				echo $property . '=' . $value . ': ' . count($i) . "\n";
		}
	}
}
