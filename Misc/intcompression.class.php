<?php

class IntCompression
{
	/**
	 * The characters to use for compression and decompression.
	 * Once changed, all compressed integers will be undecompressable!
	 */
	private static $chr = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	
	/**
	 * Compresses an integer to a human readable string.
	 * For example, 1337 will become "lz"
	 * @param int $int The integer value to compress.
	 */
	public static function Compress($int)
	{
		if ($int == 0 || !is_int($int))
			return '0';
	
		$chr = IntCompression::$chr;
		$chrl = strlen($chr);
		
		$r = '';
		$over = 0;
		$divr = $int;
		while ($divr > 0)
		{
			$over = ($divr % $chrl);
			$r .= $chr[$over];
			$divr = floor($divr / $chrl);
		}
		
		return strrev($r);
	}
	
	/**
	 * Decompresses a compressed integer that was previously compressed with this class.
	 * For example, "lz" will become 1337
	 * @param string $str The string to decompress.
	 */
	public static function Decompress($str)
	{
		$chr = IntCompression::$chr;
		$chrl = strlen($chr);
		
		$l = strlen($str);
		$r = 0;
		for ($i = 0; $i < $l; $i++)
			$r += (strpos($chr, $str[$i]) * pow($chrl, ($l - ($i + 1))));
		
		return $r;
	}
}
