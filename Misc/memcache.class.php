<?php
require_once(__DIR__ . '/consthelper.class.php');

ConstHelper::DefineDefault('PROPHP_MEMCACHE_SOCKET', true);
ConstHelper::DefineDefault('PROPHP_MEMCACHE_PORT', -1);
ConstHelper::DefineDefault('PROPHP_MEMCACHE_HOST', '127.0.0.1');
ConstHelper::DefineDefault('PROPHP_MEMCACHE_PID_DIRECTORY', '/tmp');
ConstHelper::DefineDefault('PROPHP_MEMCACHE_PID_PREFIX', 'memcached');
ConstHelper::DefineDefault('PROPHP_MEMCACHE_MAXMEMORY', '64m');
ConstHelper::DefineDefault('PROPHP_MEMCACHE_DEFAULT_EXPIRATION', '3600');

class MemCache
{
	private static $m = null;

	private $prefix;

	public function __construct($prefix)
	{
		$this->prefix = $prefix;
	}

	public function Get($key)
	{
		return self::$m->get($this->prefix . '.' . $key);
	}

	public function Set($key, $value, $expiration = PROPHP_MEMCACHE_DEFAULT_EXPIRATION)
	{
		self::$m->set($this->prefix . '.' . $key, $value, $expiration);
	}

	public function Remove($key)
	{
		self::$m->delete($this->prefix . '.' . $key);
	}

	/**
	 * @param string $name Name of the cache bucket.
	 * @param boolean $memcached_start Whether to attempt to start memcached.
	 * @param integer $memcached_port Port to start memcached on, -1 for autodecide.
	 * @param string $memcached_host Host for memcached to listen on.
	 * @return MemCache MemCache object.
	 */
	public static function Load($name, $memcached_start = true)
	{
		if (self::$m === null)
			self::Setup();

		if (!self::memcachedAlive() && $memcached_start)
		{
			if (defined('PROPHP_MEMCACHE_SOCKET') && strlen(PROPHP_MEMCACHE_SOCKET) > 0)
				shell_exec(sprintf(
					'nohup memcached -s %s -P %s -m %s -d >/dev/null 2>&1 &',
					self::autoGetSocket(),
					self::autoGetPid(),
					PROPHP_MEMCACHE_MAXMEMORY
				));
			else
				shell_exec(sprintf(
					'nohup memcached -p %d -l %s -P %s -m %s -d >/dev/null 2>&1 &',
					self::autoGetPort(),
					PROPHP_MEMCACHE_HOST,
					self::autoGetPid(),
					PROPHP_MEMCACHE_MAXMEMORY
				));
			
			$timeout = time() + 3;
			do
			{
				usleep(15333);
				if (self::memcachedAlive())
					break;
			}
			while (time() < $timeout);

			if (!self::memcachedAlive())
				return false;
		}

		return new self($name);
	}

	public static function Shutdown()
	{
		shell_exec(sprintf(
			'kill %d',
			file_get_contents(self::autoGetPid())
		));
	}

	public static function Setup()
	{
		self::$m = new Memcached();

		if (defined('PROPHP_MEMCACHE_SOCKET') && strlen(PROPHP_MEMCACHE_SOCKET) > 0)
			self::$m->addServer(self::autoGetSocket(), 0);
		else
			self::$m->addServer(PROPHP_MEMCACHE_HOST, self::autoGetPort());
	}

	private static function memcachedAlive()
	{
		if (!is_file(self::autoGetPid()))
			return false;
		$pid = trim(file_get_contents(self::autoGetPid()));
		if (is_dir('/proc/' . $pid))
			return true;
		return false;
		//$status = self::$m->getStats();
		//$i = PROPHP_MEMCACHE_HOST . ':' . self::autoGetPort();
		//return (isset($status[$i]) && $status[$i]["pid"] > 0);
	}

	private static function autoGetPid()
	{
		return PROPHP_MEMCACHE_PID_DIRECTORY
			. '/' . PROPHP_MEMCACHE_PID_PREFIX
			. '-' . md5(__FILE__)
			. '.pid';
	}

	private static function autoGetSocket()
	{
		return PROPHP_MEMCACHE_SOCKET === true
			? '/tmp/memcached-' . md5(__FILE__) . '.sock'
			: PROPHP_MEMCACHE_SOCKET;
	}

	private static function autoGetPort()
	{
		if (PROPHP_MEMCACHE_PORT > 0)
			return PROPHP_MEMCACHE_PORT;

		$port = 11212;
		$port_offset = 0;
		$path = realpath(__FILE__);
		
		for ($i = 0; $i < strlen($path); $i++)
			$port_offset ^= ord($path[$i]);
		
		return ($port + $port_offset);
	}
}