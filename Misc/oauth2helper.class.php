<?php

/**
 * Uses League OAuth2-Client to handle a login (external classes required).
 * Puts user data in $_SESSION['userinfo'] and keeps the access token in $_SESSION['accessToken'].
 */
class OAuth2Helper
{
	private const persistentCookieName = 'oa2rfshtkn';
	private const persistentCookieLifetime = 60*60*24*30;

	private $oauth2_provider;

	public function __construct(\League\OAuth2\Client\Provider\GenericProvider $oauth2Provider)
	{
		$this->oauth2_provider = $oauth2Provider;
	}

	public function HandleLoginCode() : void
	{
		if (!isset($_GET['code']))
			return;

		try
		{
			$accessToken = $this->oauth2_provider->getAccessToken('authorization_code', [
				'code' => $_GET['code']
			]);
			$_SESSION['accessToken'] = $accessToken;
			$_SESSION['userinfo'] = $this->oauth2_provider->getResourceOwner($accessToken)->ToArray();

			$this->setPersistentCookie($accessToken);

			header('Location: /');
			exit;
		}
		catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $ex)
		{
			header('Location: /?loginFailed=1');
			exit;
		}
	}

	public function VerifyLogin() : bool
	{
		if (!isset($_SESSION['accessToken']))
		{
			if (isset($_COOKIE[self::persistentCookieName]))
			{
				$newAccessToken = $this->refreshToken($_COOKIE[self::persistentCookieName]);
				if ($newAccessToken === false)
				{
					$this->unsetPersistentCookie();
					return false;
				}

				$_SESSION['accessToken'] = $newAccessToken;

				try
				{
					if (!isset($_SESSION['userinfo']))
						$_SESSION['userinfo'] = $this->oauth2_provider->getResourceOwner($newAccessToken)->ToArray();
				}
				catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $ex)
				{
					$this->Logout();
					return false;
				}

				$this->setPersistentCookie($newAccessToken);
				return true;
			}

			return false;
		}

		if ($_SESSION['accessToken']->hasExpired())
		{
			$newAccessToken = $this->refreshToken($_SESSION['accessToken']->getRefreshToken());
			if ($newAccessToken === false)
			{
				$this->Logout();
				return false;
			}
			
			$_SESSION['accessToken'] = $newAccessToken;
		}

		return true;
	}

	public function Logout($logoutUri = "") : void
	{
		unset($_SESSION['accessToken'], $_SESSION['userinfo']);
		$this->unsetPersistentCookie();

		if (strlen($logoutUri > 0))
		{
			header('Location: ' . $logoutUri);
		}
	}

	private function setPersistentCookie($accessToken)
	{
		$accessTokenValues = $accessToken->getValues();
		if (isset($accessTokenValues['refresh_expires_in'])
			&& $accessTokenValues['refresh_expires_in'] > (int)ini_get('session.gc_maxlifetime'))
		{
			$cookieLifetime = $accessTokenValues['refresh_expires_in'] == 0
				? self::persistentCookieLifetime
				: $accessTokenValues['refresh_expires_in'];
			setcookie(self::persistentCookieName, $accessToken->getRefreshToken(), [
				'expires' => time() + self::persistentCookieLifetime,
				'secure' => true
			]);
		}
	}

	private function unsetPersistentCookie()
	{
		if (isset($_COOKIE[self::persistentCookieName]))
		{
			setcookie(self::persistentCookieName, null, -1);
			unset($_COOKIE[self::persistentCookieName]);
		}
	}

	private function refreshToken($refreshToken)
	{
		try
		{
			$newAccessToken = $this->oauth2_provider->getAccessToken('refresh_token', [
				'refresh_token' => $refreshToken		
			]);
			return $newAccessToken;
		}
		catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $ex)
		{
			$this->Logout();
			return false;
		}
	}
}