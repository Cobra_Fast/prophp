<?php

class OOPHelper
{
	public static function Cast($object, $class_name)
	{
		if (!is_object($object))
			return new stdClass();
		
		if (empty($class_name) || !class_exists($class_name))
			return $object;
		
		$is_cast_down = true;
		
		$br = new ReflectionClass(get_class($object));
		
		$tr = new ReflectionClass($class_name);
		$target = $tr->newInstanceWithoutConstructor();
		foreach ($br->getProperties() as $p)
		{
			if ($tr->hasProperty($p->getName()))
			{
				$p->setAccessible(true);
				$tr->getProperty($p->getName())->setValue($target, $p->getValue($object));
			}
			else
				$is_cast_down = false;
		}
		
		if ($is_cast_down)
		{
			if ($tr->hasMethod('__castedDown'))
				$tr->getMethod('__castedDown')->invoke($target, $br);
		}
		else
		{
			if ($tr->hasMethod('__castedUp'))
				$tr->getMethod('__castedUp')->invoke($target, $br);
		}
		
		return $target;
	}
}