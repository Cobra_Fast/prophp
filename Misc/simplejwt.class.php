<?php

class SimpleJWT
{
	public static $header = 'SJWT';
	public static $algo = 'sha256';
	public static $key = '';

	public static function SignToken($payload, $key = '')
	{
		$key = strlen($key) === 0 ? self::$key : $key;
		if (strlen($key) <= 0)
			trigger_error('Key for SimpleJWT::SignToken is emptystring!', E_USER_ERROR);
		if (strlen($key) < 16)
			trigger_error('Using weak key for SimpleJWT::SignToken!', E_USER_WARNING);
		$jpayload = json_encode($payload);
		$signature = hash_hmac(self::$algo, $jpayload, $key);
		return self::$header
			. '.'
			. self::base64_encode_nopad(gzdeflate($jpayload))
			. '.'
			. self::base64_encode_nopad($signature);
	}

	public static function VerifyToken($token, $key = '')
	{
		$key = strlen($key) === 0 ? self::$key : $key;
		if (strlen($key) <= 0)
			trigger_error('Key for SimpleJWT::SignToken is emptystring!', E_USER_ERROR);

		$segments = explode('.', $token);

		if (count($segments) !== 3 || $segments[0] !== self::$header)
			return false;

		$jpayload = gzinflate(base64_decode($segments[1]));
		if ($jpayload === false)
			return false;

		$controlsig = self::base64_encode_nopad(
			hash_hmac(self::$algo, $jpayload, $key)
		);
		if ($controlsig !== $segments[2])
			return false;

		return json_decode($jpayload, true);
	}

	private static function base64_encode_nopad($str)
	{
		return str_replace('=', '', base64_encode($str));
	}
}
