<?php

class TransientCache
{
	private $data;
	
	public function __construct()
	{
		$this->data = array();
	}
	
	public function Set($key, $value)
	{
		if ($value === false)
			$this->Remove($key);
		else
			$this->data[$key] = $value;
	}
	
	public function SetAll($array)
	{
		$this->data = $array;
	}
	
	public function Get($key)
	{
		// always return a cache miss if disabled
		if (self::isCacheDisabled())
			return false;
		
		if (isset($this->data[$key]))
			return $this->data[$key];
		return false;
	}
	
	public function GetAll()
	{
		return $this->data;
	}

	public function Remove($key)
	{
		if (isset($this->data[$key]))
			unset($this->data[$key]);
	}
	
	public function Cache($key, $valueGetter, $params = null)
	{
		if (!is_callable($valueGetter))
			trigger_error('$valueGetter needs to be callable.', E_USER_ERROR);

		if (($result = $this->Get($key)) !== false)
			return $result;

		$result = call_user_func_array($valueGetter, $params);

		if ($result !== false)
			$this->Set($key, $result);

		return $result;
	}

	public static function Load($name)
	{
		// litter pointless empty objects if disabled
		if (self::isCacheDisabled())
			return new self();
		
		global $___transientcache;
		
		if (empty($___transientcache))
			$___transientcache = array();
		
		if (isset($___transientcache[$name]))
			return $___transientcache[$name];
		
		$result = new self();
		
		$___transientcache[$name] = $result;
		
		return $result;
	}
	
	private static function isCacheDisabled()
	{
		return (defined('DISABLE_TRANSIENT_CACHE') && DISABLE_TRANSIENT_CACHE == true);
	}
}
