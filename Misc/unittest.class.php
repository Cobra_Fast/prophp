<?php

class UnitTest
{
	public static function Test($name, $function, $exception_expected = false)
	{
		echo "       $name";

		try
		{
			call_user_func($function);
		}
		catch (Exception $ex)
		{
			if ($exception_expected)
				self::pass();
			else
				self::fail("Unexpected exception caught: $ex");
		}
	}

	public static function Assert($expression)
	{
		if ($expression)
			self::pass();
		else
			self::fail("Expression evaluated to false.");
	}

	public static function AssertEquals($expected, $actual)
	{
		if ($actual == $expected)
			self::pass();
		else
			self::fail("Expected $expected but got $actual.");
	}

	public static function AssertEqualsStrict($expected, $actual)
	{
		if ($actual === $expected)
			self::pass();
		else
			self::fail("Expected $expected but got $actual.");
	}

	private static function pass()
	{
		echo "\r[ \033[0;32mOK\033[0m ] \n";
	}

	private static function fail($msg)
	{
		echo "\r[\033[0;31mFAIL\033[0m] \n";
		echo $msg . "\n";
	}
}