<?php

class Thread
{
	private $pid = -1;
	private $function = -1;
	private $functionIsObject = false;
	private $goodToGo = false;
	
	/**
	 * Whether a global variable is needed for execution or not.
	 * @var <bool|string> false or name of a variable to use globally (for example class instances) 
	 */
	public $needsGlobal = false;
	
	/**
	 * Set up a new thread to run. Running objects is EXPERIMENTAL.
	 * @param string $function Function name or object->method to call. Example: 'my_super_func', 'MyClass::makeStatic', 'InitializedClass->makeSth' (set needsGlobal for this!)
	 * @param bool $functionIsObject Set this to true if you're calling something in a class instance.
	 */
	public function __construct($function, $functionIsObject = false)
	{
		if (Thread::checkThreading())
			$this->goodToGo = true;
		else
			trigger_error('Threading is not supported on this current installation.', E_USER_WARNING);
		
		$this->function = $function;
		$this->functionIsObject = $functionIsObject;
	}
	
	/**
	 * Returns the function that will be executed.
	 * @return string The function that was passed.
	 */
	public function getFunction()
	{
		return $this->function;
	}
	
	/**
	 * Whether the function is an object or not.
	 * @return bool Function is object (class->method)
	 */
	public function getFunctionIsObject()
	{
		return $this->functionIsObject;
	}
	
	/**
	 * Gets the threads PID
	 * @return int PID 
	 */
	public function getPid()
	{
		if ($this->pid >= 0) return $this->pid;
		return false;
	}
	
	/**
	 * Start the thread! All parameters passed, will be passed on to the callback function/object.
	 * @global mixed needsGlobal set through Thread::needsGlobal
	 * @return bool Returns false if something failed. Returns nothing on success. 
	 */
	public function start()
	{
		if (!$this->goodToGo)
		{
			trigger_error('Threading is not supported with the current setup, executing delegate synchroneously.', E_USER_WARNING);
			
			if ($this->functionIsObject)
				eval($this->function . '();');
			else
				call_user_func_array($this->function, func_get_args());
			
			return false;
		}
		
		if ($this->needsGlobal !== false)
		{
			global $$this->needsGlobal;
		}
			
		$pid = pcntl_fork();
		if ($pid == -1)
		{
			return false;
		}
		else if ($pid)
		{
			$this->pid = $pid;
		}
		else
		{
			pcntl_signal(SIGTERM, array($this, 'signalHandler'));
			$args = func_get_args();
			if (empty($args))
			{
				if ($this->functionIsObject)
					eval($this->function . '();');
				else
					call_user_func($this->function);
			}
			else
			{
				if ($this->functionIsObject)
				{
					eval($this->function . '(\'' . serialize($args) . '\');');
				}
				else
					call_user_func_array($this->function, $args);
			}
			exit(0);
		}
	}
	
	/**
	 * Alias for start, use start if you want to pass arguments.
	 */
	public function run()
	{
		$this->start();
	}
	
	/**
	 * Abort the thread.
	 * @param int $signal Signal to send. (SIGTERM (15))
	 * @param bool $wait Wait for the thread to finish before continuing script execution. (false)
	 */
	public function abort($signal = SIGTERM, $wait = false)
	{
		if ($this->isAlive())
		{
			posix_kill($this->pid, $signal);
			if ($wait)
				pcntl_waitpid($this->pid, $status = 0);
		}
	}
	
	/**
	 * Alias for abort.
	 */
	public function stop($signal = SIGTERM, $wait = false)
	{
		$this->abort($signal, $wait);
	}
	
	/**
	 * Whether the thread is alive or not.
	 * @return bool True if it's alive, False if not.
	 */
	public function isAlive()
	{
		return (pcntl_waitpid($this->pid, $status = 0, WNOHANG | WUNTRACED) === 0);
	}
	
	/**
	 * Handles POSIX-SIG*s for terminating. Will only be called within children.
	 * @param int $sig 
	 */
	private function signalHandler($sig)
	{
		switch ($sig)
		{
			case SIGTERM: exit(0); break;
		}
	}
	
	/**
	 * Checks if threading is available.
	 * @return bool True if threading is available, otherwise False.
	 */
	public static function checkThreading()
	{
		$required_functions = array(
			'pcntl_fork',
			'pcntl_waitpid',
			'posix_kill'
		);
		
		foreach($required_functions as $function) 
		{
			if (!function_exists($function)) 
			{
				return false;
			}
		}
		
		if (!defined('STDOUT'))
			return false;
		
		return true;
	}
}

/**
 * Function needed by Thread to enquote string parameters.
 * @param mixed $subject The subject to enquote if it's a string.
 * @return mixed Enquoted string or raw $subject.
 */
function _quoteString($subject)
{
	if (is_string($subject))
		return "'" . $subject . "'";
	else
		return $subject;
}
