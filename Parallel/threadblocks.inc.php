<?php
require_once(realpath(dirname(__FILE__)) . '/threadmanager.class.php');

/**
 * Parallelizes a for loop.
 * WARNING: Using resources or altering outside-variables inside this loop is a bad idea. This is because this function uses forks, not threads; Use shm_* or memcached if anything.
 * @param int $i_start Start value for the counting variable
 * @param function $i_bool_callback Evaluation function for the counting variable, should return boolean
 * @param function $i_inc_callback Incrementation function for the counting variable by reference
 * @param array $body_arguments Following arguments will be passed after counting variable.
 * @param function $body_callback Body part of the for loop. Get's passed the counting variable as first argument.
 * @param int $max_threads Maximum number of threads to spawn. Defaults to 2.
 * @return boolean Instantiation success or failure.
 */
function parallel_for($i_start, $i_bool_callback, $i_inc_callback, $body_arguments, $body_callback, $max_threads = 2)
{
	if (!AS_Thread::checkThreading() || !is_callable($i_bool_callback) || !is_callable($i_inc_callback) || !is_callable($body_callback) || !is_array($body_arguments))
		return false;
	
	$oTM = new ThreadManager();
	$oTM->maxThreads = $max_threads;
	
	for ($i = $i_start; $i_bool_callback($i); $i_inc_callback($i))
	{
		$oTM->AddStartWait(new Thread($body_callback), array_merge(array($i), $body_arguments));
	}
	
	$oTM->WaitAllFinished();
	return true;
}

/**
 * Parallelizes a while loop.
 * WARNING: Using resources or altering outside-variables inside this loop is a bad idea. This is because this function uses forks, not threads. Use shm_* or memcached if anything.
 * @param type $bool_arguments
 * @param type $bool_callback
 * @param type $body_arguments
 * @param type $body_callback
 * @param type $max_threads
 * @return boolean
 */
function parallel_while($bool_arguments, $bool_callback, $body_arguments, $body_callback, $max_threads = 2)
{
	if (!AS_Thread::checkThreading() || !is_callable($bool_callback) || !is_array($bool_arguments) || !is_callable($body_callback) || !is_array($body_arguments))
		return false;
	
	$oTM = new ThreadManager();
	$oTM->maxThreads = $max_threads;
	
	while (call_user_func_array($bool_callback, $bool_arguments))
	{
		$oTM->AddStartWait(new Thread($body_callback), array_merge($body_arguments));
	}
	
	$oTM->WaitAllFinished();
	return true;
}
