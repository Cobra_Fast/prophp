<?php
require_once(realpath(dirname(__FILE__)) . '/thread.class.php');

class ThreadManager
{
	public $maxThreads = 2;

	public $waitTime = 1000; // times are in microseconds (0.000001 s)
	public $waitRand = 500;

	private $aoThread = array();
	
	public function __construct()
	{
		
	}
	
	/**
	 * Get a copy of the internal threads array.
	 * @return array(Thread) Threads 
	 */
	public function GetThreads()
	{
		$tmp = $this->aoThread;
		return $tmp;
	}
	
	/**
	 * Add a thread to management.
	 * @param Thread $oThread thread
	 * @return bool success (true) or failure (false)
	 */
	public function Add($oThread)
	{
		if (count($this->aoThread) < $this->maxThreads && $oThread instanceof Thread)
		{
			$this->aoThread[] = $oThread;
			return true;
		}
		else return false;
	}
	
	/**
	 * Starts a thread and adds it to management
	 * @param Thread $oThread thread
	 * @param array $aArgs arguments passed to the thread
	 * @return bool success (true) or failure (false)
	 */
	public function AddStart($oThread, $aArgs)
	{
		if (count($this->aoThread) < $this->maxThreads && $oThread instanceof Thread && is_callable(array($oThread, 'start')))
		{
			call_user_method_array('start', $oThread, $aArgs);
			$this->aoThread[] = $oThread;
			return true;
		}
		else return false;
	}
	
	public function AddStartWait($oThread, $aArgs)
	{
		$this->WaitSlotFree();
		$this->AddStart($oThread, $aArgs);
	}
	
	/**
	 * Wait till all managed threads have finished.
	 */
	public function WaitAllFinished()
	{
		$this->Cleanup();
		
		if (count($this->aoThread) > 0)
		{
			usleep($this->waitTime + mt_rand(0, $this->waitRand));
			$this->WaitAllFinished();
		}
	}
	
	public function Cleanup()
	{
		foreach ($this->aoThread as $ID => $oThread)
		{
			if (!$oThread->isAlive() || !($oThread instanceof Thread))
			{
				@$oThread->abort();
				unset($this->aoThread[$ID]);
			}
		}
	}
	
	/**
	 * Check if a slot is free
	 * @return bool slot free (true) or not (false)
	 */
	public function SlotFree()
	{
		$this->Cleanup();
		
		if (count($this->aoThread) < $this->maxThreads)
			return true;
		return false;
	}

	/**
	 * Wait until there is a slot free
	 * @return bool always returns true (at least it should)
	 */
	public function WaitSlotFree()
	{
		while (!$this->SlotFree())
			usleep($this->waitTime + mt_rand(0, $this->waitRand));

		return true;
	}
	
	/**
	 * Kills/aborts a managed thread.
	 * @param short $PID The PID
	 * @return bool success (true) or failure (false)
	 */
	public function Kill($PID)
	{
		foreach ($this->aoThread as $ID => $oThread)
		{
			if ($oThread->getPid() == $PID)
			{
				$oThread->abort();
				return true;
			}
		}
		
		return false;
	}
}
