prophp
======

Professional and advanced tools and structures for PHP applications.

## License

Published under the terms of the Creative Commons Attribution-ShareAlike 3.0 Unported license as per <http://creativecommons.org/licenses/by-sa/3.0/deed>.

## Debug
Contains a super simple server and sending class. Handy for debugging background tasks and other intrasparent actions.

Start the 'debug_server.php' CLI-script on your linux terminal.

Then setup the constant 'DEBUG_SERVER_ADDRESS' to contain IP and port of your debug server (default port is 50252).

Then, in code simply include 'debug_client.inc.php' and start sending info to your terminal:

```php
$variable = new MyClass();
Debug::Write($variable);
Debug::Write('Works with strings, arrays and objects!');
```

## Parallel
Offers classes and functions for parallelization on CLI. This is not real threading, it's actually forking.
Child processes will work with a complete copy of everything and not be able to manipulate the parents memory, as opposed to real threading.
If you must have shared memory, look into the ```shm_*()``` functions of PHP.

*Important*: Running these things from within a webserver can be devastating (although these classes fall back to sync mode in case something's wrong).

The following are usage examples.

### Thread
```php
function foo($what) { echo "Hello $what!"; }

$t = new Thread('foo');
$t->start('World');
```
```php
class bar
{
  public static function derp($what)
  {
    echo "Hello $what!";
  }
}
$t = new Thread('bar::derp', true);
$t->start('World');
```
```php
class rab
{
  public function herp($what)
  {
    echo "Hello $what!";
  }
}
global $o;
$o = new rab();
$t = new Thread('$o->herp', true);
$t->needsGlobal = 'o';
$t->start('World');
```

### ThreadManager
```php
$tm = new ThreadManager();
$tm->maxThreads = 4; // default: 2
$tm->AddStartWait(new Thread('foo')); // starts the new thread as soon as a slot is free
$tm->WaitAllFinished();
```
### Threadblocks
```php
parallel_for(0, function ($i) { return ($i < 10); }, function (&$i) { $i++; }, array('World'), function ($i, $str) {
  echo "Hello $str No.$i!\n";
});
```
```php
parallel_while(array($extvar), function ($extvar) { return ($extvar == false); }, array('World'), function ($str) {
  echo "Hello $str!\n";
});
```

## Misc
### IntCompression
Offers two static methods to compress and decompress an integer in a human readable format.
Great for creating short URLs on IDs.

Use caution when modifying the usable characters property of this class, after changing it all previously compressed integers will be undecompressable!

```php
IntCompression::Compress($c = 1337); // => lz
IntCompression::Decompress($c); // => 1337
```

### IndexedObjectList
A class that stores an array of objects of the same type and builds dictionary like indexes of specified properties for lightning fast searching.
Using it to search through properties is a lot faster than nesting an if-construct within a foreach loop.

When no properties are indexed it automatically falls back to the slow foreach-if schema, so behaviour doesn't break.

```php
class foo
{
  public $bar;
}

$aFoo = array(/* a lot of foo in here */);
$aiFoo = new IndexedObjectList($aFoo, array('bar'));
foreach ($aiFoo->GetAllByProperty('bar', 'derp') as $oFoo)
{
  /* doing things with the found objects */
}
```

### OOPHelper
A class that aids with polymorphic structures and other frequent OOP tasks. Also which are not natively available through PHP.

When using the Cast() function, property values are copied between commonly named properties.
The function does not check the inheritance chain, so casting between unrelated classes should be possible.
The function creates a new object, so you can keep the old instance.
The function invokes __castedUp() in the new instance unless at least one property could not be copied, then it invokes __castedDown().
If the passed base object variable does not contain an object, a new instance of stdClass is returned.
If the passed desired classname is empty or does not contain a valid class name, the passed object is returned.
```php
class foo
{
  public $a;
  private $b;
  
  public function __castedDown(ReflectionClass $from)
  {
    echo $from->getName();
  }
}

class bar extends foo
{
  private $c;
  
  public function __castedUp(ReflectionClass $from)
  {
    echo $from->getName();
  }
}

$a = new foo();
$b = OOPHelper::Cast($b, 'bar'); // __castedUp will be called

$c = new bar();
$d = OOPHelper::Cast($c, 'foo'); // __castedDown will be called, since bar->c could not be copied
```

### TransientCache
Provides key-value (and arbitrary object) caching in named stores. 
Intended to be used in webserver scripts with a short lifetime as it abuses a global variable (`$___transientcache`) to store all instances. Works well in `DbThing->Select($id)` kind of situations where the same object might be needed several times.

The cache can be disabled (i.e. made to always miss) by setting the constant `DISABLE_TRANSIENT_CACHE` to a truey value.

```php
function FetchObject($request_id)
{
    $cache = TransientCache::Load("example"); // load or create by name
    
    if (($result = $cache->Get($request_id)) !== false) // attempt value retrieval
        return $result; // return cached result if hit
        
    $result = $mysqlihelper->Query('SELECT * FROM things WHERE id = ?', $request_id)->fetch_object();
    
    $cache->Set($request_id, $result); // store painstakingly computed result for later reuse
    
    return $result;
}
```

Above code does not need to be boilerplated (as much) as there is a handy method that does the same thing.  
Note: The result fetching function should return `false` on fetch miss.
```php
function FetchObject($request_id)
{
    return TransientCache::Load("example")->Cache(
        $request_id,
        function ($mysqlihelper) use ($request_id)
        {
            return ($r = $mysqlihelper->Query('SELECT * FROM things WHERE id = ?', $request_id)) instanceof mysqli_result
                && $r->num_rows > 0 ? $r->fetch_object() : false;
        },
        array($this->getMysqli())
    );
}
```

### MemCache
A helper class to streamline working with `memcached`. Works similarly to the aforementioned `TransientCache`. Requires the `Memcached` PECL module.

```php
function FetchObject($request_id)
{
    $cache = MemCache::Load("example");

    // ... see TransientCache
}
```

### SimpleJWT
Helper class for working with JWT tokens. This class is likely incompatible with tokens created by something else.

```php
$token = SimpleJWT::SignToken($payload1, "hunter2"); // followed by e.g. SetCookie()
$payload2 = SimpleJWT::VerifyToken($token1, "hunter2"); // => payload or false
```

### UnitTest
Rudimentary framework for unit tests. See `/Tests` directory for usage examples.

```php
UnitTest::Test("Example test", fn() {
    UnitTest::Assert(false);
    UnitTest::AssertEqualsStrict("1", 1);
})
```

## Data
### MySQLiHelper
Wraps a standard MySQLi instance.

```php
$sql = new MySQLiHelper(new mysqli('host', 'user', 'password', 'database'));
$insert_id = $sql->Query('INSERT INTO table (column, data) VALUES (?, ?)', 123, 'foo'); // supports prepared statements
$result = $sql->Query('SELECT * FROM table'); // returns mysqli_result
$sqli = $sql->GetLink(); // returns the underlying mysqli instace
```

### SimpleXMLHelper
Provides supplementary functions for working with SimpleXML.

```php
$xml = simplexml_load_file('test.xml');
print_r(SimpleXMLHelper::ToArray($xml));
```

### ProArray
Provides LINQ-like array handling. Supports `All(fn)`, `Any(fn)`, `First([fn])`, `Last([fn])`, `Single([fn])`, `Select(fn)`, `Where(fn)`, `Each(fn)`, `Reverse()`, `Distinct()`, `Min()`, `Max()`, `Average()`, `Aggregate(s, fn(s, v, i))`, `count()` (!), and array access.

Notes:
* `Single(fn)` emits a warning and returns null in case of `!=1` items.
* `count()` is not capitalized to comply with PHP's `Countable`.

```php
$array = new ProArray(array("foo", "bar", "baz"));
$array->First(fn($i) => $i == "bar"); // => bar
$array->All(fn($i) => $i[0] == 'b'); // => false
$array->Any(fn($i) => $i[0] == 'f'); // => true
// ...

// Chaining is supported and executed in order:
$array->Where(fn($i) => $i[0] == 'b')
    ->Select(fn($i) => $i[1]); // => {'a', 'a'}
```

## Math
Contains classes for common math tasks not part of PHP.

### Vector3
A three dimensional vector.

```php
$v = new Vector3(1.0, 1.5, 2.0);
$v = new Vector3(1.0); // supports short initializing, copying X to Y and Z too
$v = new Vector3(1.0, 1.5) // ... or copy Y to Z too
$v = new Vector3($v); // supports copying
$v->Length(); // calculate length from (0, 0, 0)
$v->IsZero(); // true if == (0, 0, 0)
$v->Dot(Vector2); // calculate dot product with a Vector2
$v->Multiply(2); // multiply by a scalar, returns itself
$v->Multiply($v); // multiply by a Vector3, returns itself
// ... same pattern for ->Divide(), ->Add(), and ->Substract()
$v->Inverse(); // invert (x = 1/x)
$v->Negate(); // negate (x = -x)
Vector3::sMultiply($v, 2); // multiply, returns new Vector3
// ... same pattern for ::sDivide(), ::sAdd(), and sSubstract()
```

### Vector2
A two dimensional vector. Supports all methods Vector3 does, substituting Vector3 for Vector2 in parameters.

### Color3
Extends Vector3 with specialized methods for RGB-color handling. RGB is mapped to XYZ respectively.

```php
$c = new Color3(); // see Vector3 constructor
$c->Normalize(); // clamps all values in {0..1}
$c->To255(); // returns array('r' =>, 'g' =>, 'b' =>) with values in {0..255}
$c->To255Hex(); // returns hex style color value, e.g. "#12FE34"
Color3::FromHex("#12FE34"); // parses hex style color and returns a corresponding Color3 instance
Color3::FromByte($r, $g, $b); // maps values in {0..255} to internal {0..1} representation and returns a corresponding Color3 instance
Color3::FromFloat($r, $g, $b); // synonymous to new Color3($r, $g, $b);
```
