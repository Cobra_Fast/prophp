<?php

require_once(__DIR__ . '/../Misc/unittest.class.php');
require_once(__DIR__ . '/../Misc/memcache.class.php');

$cache = null;
UnitTest::Test("Load cache", function () use (&$cache) {
	$cache = MemCache::Load("memcachetest");
	UnitTest::Assert($cache !== false);
});

UnitTest::Test("Set and Get variable", function () use (&$cache) {
	$cache->Set("foo", "bar");
	$v =  $cache->Get("foo");
	UnitTest::AssertEqualsStrict("bar", $v);
});

UnitTest::Test("Remove variable", function () use (&$cache) {
	$cache->Remove("foo");
	$v = $cache->Get("foo");
	UnitTest::AssertEqualsStrict(false, $v);
});

if (is_object($cache))
	$cache->Shutdown();
