<?php

require_once(__DIR__ . '/../Misc/simplejwt.class.php');

$data = array(
	'username' => 'johndoe42',
	'admin' => false,
	'remoteaddr' => '123.124.125.126'
);

SimpleJWT::$key = 'GiveItAGoodScrubThere!';

$bench = microtime(true);
$token = SimpleJWT::SignToken($data);
$bench = microtime(true) - $bench;
echo "$token (" . strlen($token) . ") (" . sprintf('%.2f', $bench * 1000) . " ms)\n";

$bench = microtime(true);
$test = SimpleJWT::VerifyToken($token);
$bench = microtime(true) - $bench;
var_dump($test); // should be $data
echo sprintf('%.2f', $bench * 1000) . " ms\n";

$test = SimpleJWT::VerifyToken(substr($token, 0, -1));
var_dump($test); // should be false
